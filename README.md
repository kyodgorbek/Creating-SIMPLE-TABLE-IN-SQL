# Creating-SIMPLE-TABLE-IN-SQL
CREATE DOMAIN OwonerNumber AS VARCHAR(5)
             CHECK(VALUE IN(SELECT ownerNo FROM PrivateOwner));
CREATE DOMAIN StaffNumber AS VARCHAR(5)
             CHECK (VALUE IN(SELECT staffNo FROM Staff));
	     
CREATE  DOMAIN Branch Number AS CHAR(4)
             CHECK(VALUE IN(SELECT branchNo FROM BRANCH));
CREATE DOMAIN PropertyNumber AS VARCHAR(5);
CREATE DOMAIN Street AS VARCHAR(25);
CREATE DOMAIN City AS VARCHAR(15);
CREATE DOMIAN Postcode AS VARCHAR(8)
CREATE DOMAIN PropertyType AS CHAR(1)
                CHECK(VALUE IN('B,' 'C,' 'D', 'E', 'F', 'M', 'S' );
CREATE DOMIAN propertyRooms AS SMALLINT;
              CHECK(VALUE BETWEEN | 0 AND  9999.99);

CREATE TABLE PropertyForRent(
              PropertyNo PropertyNumber NOT NULL,
	      street         Street                 NOT NULL,
	      city            City                     NOT NULL,
	      postcode    PostCode,
	      type           PropertyType       NOT NULL DEFAULT 'F',
	      rooms         PropertyType       NOT NULL DEFAULT 4,
	      rent            PropertyRent       NOT NULL DEFAULT 600,
              ownerNo     OwnerNumber     NOT NULL,
              staffNo        StaffNumber
                                 CONSTRANT StaffNoHandlingTooMuch
				   CHECK (NOT EXISTS(SELECT staffNo
				                                   From PropertyForRent
								   GROUP BY staffNo
								   HAVING COUNT(*) > 100)),
								   NOT NULL,
		branchNo    BranchNumber
		PRIMARY KEY (propertyNo),
		FOREIGN KEY (staffNo) REFERENCES Staff ON DELETE SET NULL
		                                   ON UPDATE CASCADE,
						   
	FOREIGN KEY (ownerNo) REFERENCES PrivateOwner ON DELETE NO
					      ACTION ON UPDATE CASCADE,
					      
	FOREIGN KEY (branchNo) REFERENCES Branch ON DELETE NO
	                                         ACTION ON UPDATE CASCADE);
